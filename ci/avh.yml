# Copyright (c) 2022-2023 Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

name: "Open IoT-SDK Keyword Tests"
workdir: ../
backend:
  aws:
    instance-type: c5.large
  # AMI version will be set in environmental variable
upload:
  - '**/*'
  - '**/.git/**/*'
steps:
  - run: |
      sudo apt update -y
      sudo apt install python3.8-venv python3-virtualenv -y
      python3.8 -m pip install imgtool cbor2
      python3.9 -m pip install cffi intelhex cbor2 cbor pytest pytest_asyncio pyyaml click imgtool jinja2 boto3 boto3-type-annotations jmespath
      mkdir -p workspace/cmsis_toolbox
      mkdir -p /usr/local/bin/cmsis_toolbox
      wget -q https://github.com/Open-CMSIS-Pack/cmsis-toolbox/releases/download/1.5.0/cmsis-toolbox-linux-amd64.tar.gz -O - | tar -xz -C workspace/cmsis_toolbox
      sudo cp -r workspace/cmsis_toolbox/cmsis-toolbox-linux-amd64/* /usr/local/bin/cmsis_toolbox
      PATH="/usr/local/bin/cmsis_toolbox/bin:${PATH}"
      CMSIS_PACK_ROOT="/usr/local/bin/cmsis_toolbox/pack"
      CMSIS_COMPILER_ROOT="/usr/local/bin/cmsis_toolbox/etc"
      sudo chmod -R a+rwx /usr/local/bin/cmsis_toolbox
      sudo wget -qO /usr/local/bin/yq https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64
      sudo chmod a+x /usr/local/bin/yq
      sudo apt-get -y install jq

  - run: |
      export PATH=$PATH:$HOME/.local/bin
      find . -name "*.sh" -exec chmod +x {} \;
  # build application ci/env.sh is used to pass environmental variables
  # from GitLab runner to AWS EC2 instance running AVH AMI
  - run: |
      set -x
      export PATH=$PATH:$HOME/.local/bin
      . ./ci/env.sh
      export ARM_TOOL_VARIANT=ult
      for APPLICATION in ${APPLICATIONS_TO_BUILD//,/ }
      do
        ./ats.sh build ${APPLICATION} --path ${BUILD_PATH} -a ${CREDENTIALS_PATH} --target ${TARGET} --rtos ${RTOS} -e ${ENDPOINT}
      done

  # run tests
  - run: |
      # source env/bin/activate
      export PATH=$PATH:$HOME/.local/bin
      . ./ci/env.sh
      export ARM_TOOL_VARIANT=ult
      for TEST_ID in ${PYTEST_SELECTION//,/ }
      do
          pytest -s ${TEST_ID} --build-path ${BUILD_PATH} --avh ${AVH} --credentials-path ${CREDENTIALS_PATH} --junitxml=report_"${TEST_ID//\//-}".xml
      done
      ls -la

download:
  - build/**/*.log
  - report*.xml
