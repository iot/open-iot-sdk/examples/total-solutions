# Getting started with Arm® Corstone™ SSE-310 with Cortex®-M85 and Ethos™-U55

## Unboxing

* Unpack carefully and minimize electrostatic discharges
* Install the provided SODIMM
* Ensure both `CFGSW` switches are in the up position

## Powering & preparing the board

FPGA can be programmed to be Corstone-300 or Corstone-310, the MPS3 is shipped programmed with the AN524 FPGA image which would be replaced with the AN555 image to program it to be Corstone-310 which is the only supported FPGA platform with Total Solution examples.

Download the latest FPGA image package available at <https://developer.arm.com/tools-and-software/development-boards/fpga-prototyping-boards/download-fpga-images>.
At the time of writing this document, the archive's name for the AN555 image is `AN555_SSE310_with_M85_and_U55_FPGA_for_mps3v2.0.zip`.

* Connect the provided USB cable between the board's USB A port and a host computer
* Connect one of the provided AC power cables between the board and a power source
* Press one of the `PBON` button located on opposite sides of the board
* Wait for the image to load, this should take approximately 30 seconds
* `V2M-MPS3` appears as a mass storage device on the host computer, backup the content of that volume
* Delete the content of that volume or reformat the drive (preferably to FAT16 but FAT32 can also be
  used)
* Copy the content of the `Boardfiles` directory from the downloaded archive onto the `V2M-MPS3` volume
* Once copy is complete, eject the drive and press one of the `PBRST` buttons (located on opposite
  sides of the board) to shut down the board
* Press one of the `PBON` buttons to turn on the board and load the new FPGA image
  After approximately 30 seconds the splash screen should show on the board

## Check the board is ready

The board will attach five new serial port.

* One is a DAPLink serial port. It supports resetting the target with a break signal but is not actually
  connected to any serial port of the target
* One is linked to the MCC (Motherboard Control Chip) control console
* Three are linked to the target's serial port

On Linux:

* /dev/ttyACM0: is the DAPLink interface
* /dev/ttyUSB0: is the MCC interface
* /dev/ttyUSB1 to ttyUSB3 are the target's UARTs

On Windows, it depends on whether you have the right drivers.

Connect to the DAPLink serial port and the first serial port with your favorite terminal (ex. minicom, picocom).

E.g. on Linux using minicom terminal:

* DAPLink : `minicom -D /dev/ttyACM0 -b 115200`
* MCC : `minicom -D /dev/ttyUSB0 -b 115200`
* UART : `minicom -D /dev/ttyUSB1 -b 115200`

If you got permission denied connection issues, please make sure to add your user to the `dialout` group:
```
sudo usermod -aG dialout $USER
```
Afterwards, you'll have to logout and then log back in for the group changes to take effect.

From the DAPLink interface emit a break signal. For example, using the minicom terminal you can emit a break signal
by pressing `CTRL+A` keys followed by `F` key. The other port (`dev/ttyUSB1`) should display the self-test
application's menu:

```text
Arm MPS3 FPGA Prototyping Board Test Suite
Version 1.0.4 Build date: Nov 11 2021
Copyright (C) Arm Ltd 2021. All rights reserved.
V2M-MPS3 revision C

Application Note AN555, FPGA build 2

CPU: Cortex-M85 r1p0

Summary of results
====================================
 1 : AACI (Audio CODEC)      : Not Run
 2 : CLCD                    : Not Run
 3 : TSCI (TouchScreen)      : Not Run
 4 : LEDs/Switches/Buttons   : Not Run
 5 : Ethernet                : Not Run
 6 : USB                     : Not Run
 7 : Memory                  : Not Run
 8 : Timer                   : Not Run
 9 : Real Time Clock         : Not Run
10 : QSPI                    : Not Run
11 : CDE - TRIG              : Not Run

Select the test you wish to run. (X - Exit)

Choice:
```

## Loading custom application

### Method 1

1. Copy your application binary to `/path/to/V2M-MPS3/SOFTWARE/my_bin.bin` where `my_bin.bin` is the name of your application binary file.

    **Note**: The filename must not exceed 8 characters long and a file extension no longer than 3 characters long.
1. Open `/path/to/V2M-MPS3/MB/<MODEL>/AN555/images.txt` where _`MODEL`_ is either `HBI0309B` or `HBI0309C` depending on your board model.
1. In the file, modify the value of the `IMAGE0FILE` attribute to be the path to your binary relative to `/path/to/V2M-MPS3/` such as follows:
  ```text
  IMAGE0FILE: \SOFTWARE\my_bin.bin ; - my custom application binary
  ```
1. Safely eject the `V2M-MPS3` mass storage device.
1. Reboot the board using one of the `PBON` buttons.

Outcome:  The board reboots and starts running your application after approximately 30 seconds. You can now reset the application with one of the `PBRST` buttons.

### Method 2
Run the GDB server `pyocd gdbserver --persist`. Start GDB and attach to the server:

```sh
arm-none-eabi-gdb /path/to/your/app.elf -ex 'target extended-remote :3333' -ex 'load'
```

---
**Notes**

GDB can only load ELF files generated by the `GCC_ARM` toolchain.
Your custom application is unloaded from RAM when you press one of the `PBON` buttons.
