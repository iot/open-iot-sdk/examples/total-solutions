/*
 *    Copyright (c) 2023 ARM Limited
 *    All rights reserved.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

#ifdef    CMSIS_device_header
#include  CMSIS_device_header
#else
#include "cmsis_compiler.h"
#endif
#include "cmsis_os2.h"

#if (( defined(__CC_ARM) || \
      (defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050))) && \
      !defined(__MICROLIB))

extern void _platform_post_stackheap_init (void);
__WEAK void _platform_post_stackheap_init (void) {
  if (osKernelInitialize() != osOK)
  {
    abort();
  }
}

// Check if Kernel has been started
static uint32_t os_kernel_is_active (void) {
  static uint8_t os_kernel_active = 0U;

  if (os_kernel_active == 0U) {
    if (osKernelGetState() > osKernelReady) {
      os_kernel_active = 1U;
    }
  }

  return (uint32_t)os_kernel_active;
}

// Mutex identifier
typedef void *mutex;

// __USED function attribute is used with all mutex control functions as it enforce the linker to
// retain the function in the object file by which our version of these functions would be used
// rather than using the empty definition of these functions which would result in serial prints
// corruption.

// Initialize mutex
__USED
int _mutex_initialize(mutex *m);
int _mutex_initialize(mutex *m) {
  int ret;
  *m = osMutexNew(NULL);
  if (*m != NULL) {
    ret = 1;
  } else {
    ret = 0;
  }

  return ret;
}

// Acquire mutex
__USED
void _mutex_acquire(mutex *m);
void _mutex_acquire(mutex *m) {
  if (os_kernel_is_active() != 0U) {
    (void)osMutexAcquire(*m, osWaitForever);
  }
}

// Release mutex
__USED
void _mutex_release(mutex *m);
void _mutex_release(mutex *m) {
  if (os_kernel_is_active() != 0U) {
    (void)osMutexRelease(*m);
  }
}

// Free mutex
__USED
void _mutex_free(mutex *m);
void _mutex_free(mutex *m) {
  (void)osMutexDelete(*m);
}

#endif
