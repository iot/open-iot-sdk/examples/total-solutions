/*
 * Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H

// TrustZone support
#define DOMAIN_NS 1

// RTOS options
#define OS_DYNAMIC_MEM_SIZE (40000 * 3)
#define OS_ROBIN_ENABLE     1

#ifdef MPS3_FPGA
#define OS_TICK_FREQ 1000
#else
// 300 tick per second on FVP gives similar timing (~85%) as 1000 ticks did on the FPGA
#define OS_TICK_FREQ 300
#endif

#define OS_PRIVILEGE_MODE 1

#endif // RTE_COMPONENTS_H
